FROM python:3.9.0-alpine
WORKDIR /project
COPY requirements.txt /project
COPY *.py /project/
RUN pip install -r requirements.txt
EXPOSE 5000
ENTRYPOINT [ "flask"]
CMD [ "run", "--host", "0.0.0.0" ]