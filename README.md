# Tech decisions

## Backend Server

I used python with Flask to make the API. Python is a language I already used for scripting, but no for backend development, so I try it out, seems to be pretty cool, on the other hand, I like interpreted languages, that do not have to be compiled. I

## Kubernetes Cluster
For time and pricing reasons I choose a local kubernetes cluster (Minikube).
Anyway, I have worked with AWS and I have experience with it and feel more comfortable.
I have used other Kubernetes managed services from others cloud provider, and I already have Kubernetes experience.
I have experience with multiple docker container registry also (ECR, JFrog, Gitlab Registry, ACR)  

## CI/CD tool

I used GitLab for code versioning, and it has a native CI/CD tool, GitLab CI, I think it is a nice tool, already integrated with the code repo, which has a lots of benefits and it allows you to make and achieve a lot of things.
It also allows you to have your own runner on your machine instead of within the cloud. That is the approach I have used for this test.
But you can also, use the cloud one and integrate it with you cloud provider deployment and build, which I have also experience.