import json
from flask import Flask, request, make_response, jsonify

app = Flask(__name__)


@app.route('/helloworld', methods=['GET'])
def hello_world():
    return make_response(
        jsonify(
            hello='world',
        ),
        200
    )
